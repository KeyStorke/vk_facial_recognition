from vkwave.api import API, Token
from vkwave.api.token.token import UserSyncSingleToken
from vkwave.client import AIOHTTPClient
from typing import FrozenSet
import logging
from vk_throttler import VkClientThrottler
from multiprocessing import Queue

logger = logging.getLogger('FG')


class VkProfileParser:
    def __init__(self, auth_token: str):
        token = Token(auth_token)
        self.token = UserSyncSingleToken(token)
        self.api_session = API(tokens=self.token, clients=AIOHTTPClient())
        self.api = self.api_session.get_context()
        self.tasks_queue = VkClientThrottler(requests_per_sec=3)
        self.results_queue = Queue()

    async def _get_photos_links(self):
        user_id = self.tasks_queue.get()
        all_photos = await self.api.photos.get_all(owner_id=user_id)
        biggest_photos = set()
        for item in all_photos.response.items:
            biggest_photos.add(item.sizes[-1].url)
        self.results_queue.put(biggest_photos)
        return

    async def get_all_photos_links_by_user(self, user_id: int, _counter=0) -> FrozenSet[str]:
        self.tasks_queue.put(user_id)
        await self._get_photos_links()
        return self.results_queue.get()
