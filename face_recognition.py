"""
 - full code https://gitlab.com/KeyStorke/dlib_wrap
    todo: build with rust modules and publish on pypi
    todo: make docs
 - time optimizations ideas https://gitlab.com/KeyStorke/camera_agent
    todo: really need to implement that on python. Or just wrap
 - models https://gitlab.com/KeyStorke/models
    todo: need to train new models with new faces
"""

import dlib
import numpy as np
from typing import Iterable, List
from PIL import Image
import io


class FaceRecognizer:
    def __init__(self, predictor_path='models/shape_predictor_5_face_landmarks.dat', num_of_jitters=0,
                 face_rec_model_path='models/dlib_face_recognition_resnet_model_v1.dat'):
        self.cnn_detector = dlib.cnn_face_detection_model_v1('models/mmod_human_face_detector.dat')
        self.hog_detector = dlib.get_frontal_face_detector()
        self.error_counter = 0
        self.last_error = None
        self.shape_predictor = dlib.shape_predictor(predictor_path)
        self.num_of_jitters = num_of_jitters
        self.face_recognizer = dlib.face_recognition_model_v1(face_rec_model_path)

    def get_dets_from_image(self, image: np.ndarray, cnn=True):
        if cnn:
            # CNN
            try:
                dets = self.cnn_detector(image, 1)
            except Exception as e:
                self.error_counter += 1
                self.last_error = e
                dets = self.hog_detector(image, 1)
        else:
            # HOG
            dets = self.hog_detector(image, 1)
        return dets

    def count_num_of_faces(self, image: bytes, cnn=True) -> int:
        img = self.decode_image_to_nparray(image)
        dets = self.get_dets_from_image(img, cnn)
        return len(dets)

    @staticmethod
    def decode_image_to_nparray(image: bytes):
        im = io.BytesIO(image)
        im = Image.open(im)
        im = im.convert('RGB')
        return np.array(im)
        # return cv2.imdecode(np.frombuffer(im, np.uint8), -1)

    @staticmethod
    def faces_distance(face_encodings: Iterable, face_to_compare: Iterable) -> float:
        """ get distance from two vectors (face descriptors)

        :param face_encodings: first vector
        :param face_to_compare: second vector
        :return: distance
        """
        if not list(face_encodings):
            return float('INF')

        return np.linalg.norm(np.array(face_encodings) - np.array(face_to_compare))

    def get_faces_descriptors_on_image(self, image: bytes) -> List[Iterable]:
        """ find all facials  on image

        :param image: path to first image
        :return: List of descriptors
        """
        img = self.decode_image_to_nparray(image)

        # works only with HOG algorithm
        dets = self.get_dets_from_image(img, cnn=False)

        result = list()

        for d in dets:
            shape = self.shape_predictor(img, d)
            face_descriptor = self.face_recognizer.compute_face_descriptor(img, shape, self.num_of_jitters)
            result.append(face_descriptor)
        return result
