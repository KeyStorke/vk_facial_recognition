from typing import List
from rethinkdb import RethinkDB


class PhotosCalculationsDatabase:
    def __init__(self, host='127.0.0.1', port=28015, user='admin', password='', db_name='FG'):
        self.db = RethinkDB()
        self.db_name = db_name

        credentials = dict(
            host=host,
            port=port,
            user=user,
            password=password,
            db=self.db_name
        )

        self.connection = self.db.connect(**credentials).repl()

    def add_photo(self, photo_url: str, user_id: int):
        self.connection.use('FG')
        entry = dict(
            photo_url=photo_url,
            user_id=user_id
        )
        self.db.table('photo').insert(entry).run(self.connection)

    def set_current_user_id(self, user_id: int):
        self.connection.use('FG')
        entry = dict(
            id=0,
            user_id=user_id
        )
        r = self.db.table('current_user').update(entry).run(self.connection)

    def get_current_user_id(self) -> int:
        self.connection.use('FG')
        data = self.db.table('current_user').get(0).run(self.connection)
        if data:
            return data.get('user_id', None)
        return 1

    def set_current_photo_id(self, photo_ordinal: int):
        self.connection.use('FG')
        entry = dict(
            id=0,
            photo_ordinal=photo_ordinal
        )
        r = self.db.table('current_photo').update(entry).run(self.connection)
        return r

    def get_current_photo_ordinal(self) -> int:
        self.connection.use('FG')
        data = self.db.table('current_photo').get(0).run(self.connection)
        if data:
            return data.get('photo_ordinal', 0)
        return 0

    def add_skipped_photo(self, user_id, photo_url, reason):
        self.connection.use('FG')
        entry = dict(
            photo_url=photo_url,
            reason=reason,
            user_id=user_id
        )
        self.db.table('skipped_photos').insert(entry).run(self.connection)

    def add_skipped_profile(self, user_id, reason):
        self.connection.use('FG')
        entry = dict(
            reason=reason,
            user_id=user_id
        )
        self.db.table('skipped_profile').insert(entry).run(self.connection)

    def add_face_id(self, user_id: int, face_id: List[float]):
        self.connection.use('FG')

        point_0 = face_id[0]
        point_1 = face_id[1]
        point_2 = face_id[2]
        point_3 = face_id[3]

        entry = dict(
            point_0=point_0,
            point_1=point_1,
            point_2=point_2,
            point_3=point_3,
            user_id=user_id
        )
        self.db.table('face_id').insert(entry).run(self.connection)

    def get_photos_urls(self, skip=0):
        self.connection.use('FG')
        result = self.db.table('photo').filter({}).order_by('id').skip(skip).run(self.connection)
        return result
