import time
from multiprocessing import Queue

q = Queue()


class VkClientThrottler:
    def __init__(self, requests_per_sec=3):
        self.queue = Queue()
        self.access_timings = Queue(maxsize=round(requests_per_sec))
        self.requests_per_sec = requests_per_sec

    def get(self, block=True, timeout=None):
        if self.access_timings.qsize() < self.requests_per_sec:
            self.access_timings.put_nowait(time.time())
            return self.queue.get(block, timeout)

        access_time = self.access_timings.get()
        time_diff = time.time() - access_time

        if time_diff > 1:
            self.access_timings.put_nowait(time.time())
            return self.queue.get(block, timeout)

        time.sleep(1 - time_diff)
        self.access_timings.put_nowait(time.time())
        return self.queue.get(block, timeout)

    def put(self, obj, block=True, timeout=None):
        self.queue.put(obj, block, timeout)
