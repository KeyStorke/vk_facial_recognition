import urllib.request
import logging

logger = logging.getLogger('FG')


def download_photo(url) -> bytes:
    try:
        return urllib.request.urlopen(url).read()
    except Exception as e:
        logger.error('downloading photo failed with error %s URL: %s', e, url)
        raise e
