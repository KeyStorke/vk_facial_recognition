import os
import sys
import time
import logging
from multiprocessing import Queue, Process
import signal

import asyncio
from multiprocessing.spawn import freeze_support

from vk_profile_parser import VkProfileParser
from photo_downloader import download_photo
from face_recognition import FaceRecognizer
from persistent_storage import PhotosCalculationsDatabase


def ignore_sigint():
    signal.signal(signal.SIGINT, signal.SIG_IGN)


class UserPhotoData:
    __slots__ = ('user_id', 'photo_url', 'photo')

    def __init__(self, user_id, photo_url, photo=b''):
        self.user_id: int = user_id
        self.photo_url: str = photo_url
        self.photo: bytes = photo


def setup_logging():
    logger = logging.getLogger('FG')

    ch = logging.StreamHandler()
    formatter = logging.Formatter('%(process)d - %(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    logger.setLevel(logging.INFO)
    return logger


def parse_urls(queue_urls, to: int = 999999999):
    ignore_sigint()
    logger = setup_logging()
    logger.info('URL parser started')
    token = os.environ.get('token')
    parser = VkProfileParser(token)
    loop = asyncio.get_event_loop()
    persistent = PhotosCalculationsDatabase()
    current_user_id = persistent.get_current_user_id()
    for user_id in range(current_user_id, to):
        persistent.set_current_user_id(user_id)
        try:
            urls = loop.run_until_complete(parser.get_all_photos_links_by_user(user_id))
        except Exception as e:
                logger.error('skip profile %s: %s', user_id, e)
                persistent.add_skipped_profile(user_id=user_id, reason=str(e))
                time.sleep(0.1)
                continue
        for url in urls:
            data = UserPhotoData(user_id=user_id, photo_url=url)
            queue_urls.put(data)
        time.sleep(0.1)


def download_photos(queue_urls, queue_photos):
    ignore_sigint()
    logger = setup_logging()
    logger.info('Photo downloader started')
    persistent = PhotosCalculationsDatabase()
    while True:
        data = queue_urls.get()
        try:
            photo = download_photo(data.photo_url)
        except Exception as e:
            logger.error('skip photo: %s', e)
            persistent.add_skipped_photo(photo_url=data.photo_url, user_id=data.user_id, reason=str(e))
            continue
        data.photo = photo
        queue_photos.put(data)
        time.sleep(0.01)


def filter_photos(queue_photos, queue_photos_with_faces):
    ignore_sigint()
    logger = setup_logging()
    logger.info('Photos filter started')
    recognizer = FaceRecognizer()
    persistent = PhotosCalculationsDatabase()
    while True:
        data = queue_photos.get()
        try:
            num_of_faces = recognizer.count_num_of_faces(data.photo)
        except Exception as e:
            logger.error('skip photo: %s', e)
            persistent.add_skipped_photo(photo_url=data.photo_url, user_id=data.user_id, reason=str(e))
            continue
        if num_of_faces > 0:
            data.photo = b''
            queue_photos_with_faces.put(data)
        time.sleep(0.001)


def save_results(queue_photos_with_faces):
    ignore_sigint()
    logger = setup_logging()
    logger.info('Data saver started')
    persistent = PhotosCalculationsDatabase()
    while True:
        data = queue_photos_with_faces.get()
        persistent.add_photo(photo_url=data.photo_url, user_id=data.user_id)
        time.sleep(0.01)


if __name__ == '__main__':
    freeze_support()

    main_logger = setup_logging()
    main_logger.info('starting...')

    queue_urls = Queue(maxsize=300)
    queue_photos = Queue(maxsize=50)
    queue_photos_with_faces = Queue(maxsize=300)

    url_parser_proc = Process(name='URL Parser', target=parse_urls, args=(queue_urls,), daemon=True)
    download_photos_proc = Process(name='Photo Downloader', target=download_photos, args=(queue_urls, queue_photos,),
                                   daemon=True)
    filter_photos_proc = Process(name='Filter photo', target=filter_photos,
                                 args=(queue_photos, queue_photos_with_faces,), daemon=True)
    save_results_proc = Process(name='Data Saver', target=save_results, args=(queue_photos_with_faces,), daemon=True)

    url_parser_proc.start()
    download_photos_proc.start()
    filter_photos_proc.start()
    save_results_proc.start()

    while True:
        try:
            time.sleep(0.5)
        except KeyboardInterrupt:
            time.sleep(1)
            main_logger.warning('wait for URL Parser process ')
            url_parser_proc.kill()

            main_logger.warning('wait for Photo Downloader process ')
            while not queue_urls.empty():
                time.sleep(0.1)
            main_logger.warning('Photo Downloader process works on last task...')
            time.sleep(5)
            download_photos_proc.kill()

            main_logger.warning('wait for Filter Photo process ')
            while not queue_photos.empty():
                time.sleep(0.1)
            main_logger.warning('Filter Photo process works on last task...')
            time.sleep(5)
            filter_photos_proc.kill()

            main_logger.warning('wait for Data Saver process ')
            while not queue_photos_with_faces.empty():
                time.sleep(0.1)
            main_logger.warning('Data Saver process works on last task...')
            time.sleep(5)
            save_results_proc.kill()

            main_logger.info('exit...')
            sys.exit(0)
