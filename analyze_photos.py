import logging
import signal
import sys
import time
from multiprocessing import Process, Queue
from multiprocessing.spawn import freeze_support

from typing import List

from photo_downloader import download_photo
from face_recognition import FaceRecognizer
from persistent_storage import PhotosCalculationsDatabase


def ignore_sigint():
    signal.signal(signal.SIGINT, signal.SIG_IGN)


def setup_logging():
    logger = logging.getLogger('FG')

    ch = logging.StreamHandler()
    formatter = logging.Formatter('%(process)d - %(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    logger.setLevel(logging.INFO)
    return logger


class PhotoURLWithUserId:
    __slots__ = ('url', 'user_id',)

    def __init__(self, url: str, user_id: int):
        self.url = url
        self.user_id = user_id


class PhotoWithUserId:
    __slots__ = ('photo', 'user_id',)

    def __init__(self, photo: bytes, user_id: int):
        self.photo = photo
        self.user_id = user_id


class UserIdWithFacesId:
    __slots__ = ('faces_ids', 'user_id',)

    def __init__(self, faces_ids: List[List[float]], user_id: int):
        self.faces_ids = faces_ids
        self.user_id = user_id


def data_extractor_procedure(queue_urls):
    ignore_sigint()
    logger = setup_logging()
    persistent = PhotosCalculationsDatabase()
    logger.info('Data Extractor started')
    ordinal = persistent.get_current_photo_ordinal()
    logger.info('starting from photo: %s',ordinal)
    for url_entry in persistent.get_photos_urls(skip=ordinal):
        url = url_entry['photo_url']
        user_id = url_entry['user_id']
        data = PhotoURLWithUserId(user_id=user_id, url=url)
        queue_urls.put(data)
        ordinal += 1
        persistent.set_current_photo_id(ordinal)


def download_photos(queue_urls, queue_photos):
    ignore_sigint()
    logger = setup_logging()
    logger.info('Photo downloader started')
    persistent = PhotosCalculationsDatabase()
    while True:
        data: PhotoURLWithUserId = queue_urls.get()
        try:
            photo = download_photo(data.url)
        except Exception as e:
            logger.error('skip photo: %s', e)
            persistent.add_skipped_photo(photo_url=data.url, user_id=data.user_id, reason=str(e))
            continue
        new_data = PhotoWithUserId(photo=photo, user_id=data.user_id)
        queue_photos.put(new_data)
        time.sleep(0.01)


def face_recognition_procedure(queue_photos, queue_descriptors):
    ignore_sigint()
    logger = setup_logging()
    recognizer = FaceRecognizer(num_of_jitters=5)
    logger.info('Face recognition procedure started')
    logger.info('initialization of reference faces descriptors...')

    references_paths = (
        'models/reference_faces/Einstein.jpg',
        'models/reference_faces/Gagarin.jpg',
        'models/reference_faces/Gandi.jpg',
        'models/reference_faces/Tse-tung.jpg'
    )

    references = list()

    for reference_path in references_paths:
        logger.info(f'initialization \'{reference_path}\' reference...')
        with open(reference_path, 'rb') as f:
            image = f.read()
            descriptors = recognizer.get_faces_descriptors_on_image(image)
            face_descriptor = descriptors[0]
            references.append(face_descriptor)
        logger.info(f'initialization \'{reference_path}\' reference completed')

    logger.info('reference faces descriptors initialized')

    while True:
        data: PhotoWithUserId = queue_photos.get()
        photo = data.photo
        descriptors = recognizer.get_faces_descriptors_on_image(photo)

        faces_ids = list()

        for descriptor in descriptors:
            face_id = list()
            # compare each descriptor with each reference
            for reference in references:
                distance = recognizer.faces_distance(reference, descriptor)
                if distance == float('inf'):
                    distance = 1000
                face_id.append(distance)
            faces_ids.append(face_id)

        new_data = UserIdWithFacesId(faces_ids=faces_ids, user_id=data.user_id)
        queue_descriptors.put(new_data)
        time.sleep(0.01)


def data_saver(queue_descriptors):
    ignore_sigint()
    logger = setup_logging()
    persistent = PhotosCalculationsDatabase()
    logger.info('Data saver started')
    while True:
        data: UserIdWithFacesId = queue_descriptors.get()
        for face_id in data.faces_ids:
            persistent.add_face_id(user_id=data.user_id, face_id=face_id)
        time.sleep(0.001)


if __name__ == '__main__':
    freeze_support()

    main_logger = setup_logging()
    main_logger.info('starting...')

    queue_urls = Queue(maxsize=300)
    queue_photos = Queue(maxsize=50)
    queue_faces_ids = Queue(maxsize=300)

    data_extractor_proc = Process(name='Data Extractor', target=data_extractor_procedure, args=(queue_urls,),
                                  daemon=True)

    download_photos_proc = Process(name='Photo Downloader', target=download_photos, args=(queue_urls, queue_photos,),
                                   daemon=True)
    download_photos_proc_2 = Process(name='Photo Downloader 2', target=download_photos, args=(queue_urls, queue_photos,),
                                   daemon=True)

    face_recognition_proc = Process(name='Filter photo', target=face_recognition_procedure,
                                    args=(queue_photos, queue_faces_ids,), daemon=True)

    face_recognition_proc_2 = Process(name='Filter photo 2', target=face_recognition_procedure,
                                    args=(queue_photos, queue_faces_ids,), daemon=True)

    face_recognition_proc_3 = Process(name='Filter photo 3', target=face_recognition_procedure,
                                      args=(queue_photos, queue_faces_ids,), daemon=True)

    save_results_proc = Process(name='Data Saver', target=data_saver, args=(queue_faces_ids,), daemon=True)

    data_extractor_proc.start()
    download_photos_proc.start()
    download_photos_proc_2.start()
    face_recognition_proc.start()
    face_recognition_proc_2.start()
    face_recognition_proc_3.start()
    save_results_proc.start()

    while True:
        try:
            time.sleep(0.5)
        except KeyboardInterrupt:
            time.sleep(1)
            main_logger.warning('wait for URL Parser process ')
            data_extractor_proc.kill()

            main_logger.warning('wait for Photo Downloader process ')
            while not queue_urls.empty():
                time.sleep(0.1)
            main_logger.warning('Photo Downloader process works on last task...')

            main_logger.warning('wait for Filter Photo process ')
            while not queue_photos.empty():
                time.sleep(0.1)
            main_logger.warning('Filter Photo process works on last task...')

            main_logger.warning('wait for Data Saver process ')
            while not queue_faces_ids.empty():
                time.sleep(0.1)
            main_logger.warning('Data Saver process works on last task...')
            time.sleep(5)

            download_photos_proc.kill()
            download_photos_proc_2.kill()
            face_recognition_proc.kill()
            save_results_proc.kill()
            face_recognition_proc_2.kill()
            face_recognition_proc_3.kill()

            main_logger.info('exit...')
            sys.exit(0)
