from rethinkdb import RethinkDB


class FaceFinder:
    def __init__(self, host='127.0.0.1', port=28015, user='admin', password='', db_name='FG', tolerance=0.05):
        self.db = RethinkDB()
        self.db_name = db_name

        credentials = dict(
            host=host,
            port=port,
            user=user,
            password=password,
            db=self.db_name
        )

        self.connection = self.db.connect(**credentials).repl()
        self.tolerance = tolerance

    def threshold_min(self, value):
        return value - self.tolerance

    def threshold_max(self, value):
        return value + self.tolerance

    def find_face(self, face_id):
        point_0, point_1, point_2, point_3 = face_id
        faces_ids = self.db.table('face_id').filter(
            (self.db.row['point_0'] >= self.threshold_min(point_0)) &
            (self.db.row['point_0'] <= self.threshold_max(point_0)) &
            (self.db.row['point_1'] >= self.threshold_min(point_1)) &
            (self.db.row['point_1'] <= self.threshold_max(point_1)) &
            (self.db.row['point_2'] >= self.threshold_min(point_2)) &
            (self.db.row['point_2'] <= self.threshold_max(point_2)) &
            (self.db.row['point_3'] >= self.threshold_min(point_3)) &
            (self.db.row['point_3'] <= self.threshold_max(point_3))
        ).run(self.connection)
        return faces_ids
